import 'dart:convert' as convert;
import 'package:go_pangram/models/anagram.dart';
import 'package:go_pangram/models/random_anagram.dart';
import 'package:http/http.dart' as http;

class AnagramRepository {
  Future<Anagram> isValidAnagram(String firstLetter, String secondaryLetter) async {

    if(firstLetter.isEmpty || secondaryLetter.isEmpty){
      return Anagram(firstLetter, secondaryLetter, false, 'Please input some text!');
    }
    var queryParameters = {
      'firstLetter': firstLetter,
      'secondaryLetter': secondaryLetter,
    };

    var url = Uri.https('pangram-yanu.herokuapp.com', '/validAnagram', queryParameters);
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return Anagram.fromJson(convert.jsonDecode(response.body));
    }
    return null;
  }

  Future<RandomAnagram> randomAnagram() async{
    var url = Uri.https('pangram-yanu.herokuapp.com', '/anagram');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return RandomAnagram.fromJson(convert.jsonDecode(response.body));
    }
    return null;
  }
}
