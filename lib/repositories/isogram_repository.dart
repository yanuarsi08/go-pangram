import 'dart:convert' as convert;
import 'package:go_pangram/models/isogram.dart';
import 'package:go_pangram/models/random_isogram.dart';
import 'package:http/http.dart' as http;

class IsogramRepository {
  Future<RandomIsogram> randomIsogram() async{
    var url = Uri.https('pangram-yanu.herokuapp.com', '/isogram');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return RandomIsogram.fromJson(convert.jsonDecode(response.body));
    }
    return null;
  }

  Future<Isogram> isValidIsogram(String letter) async {

    if(letter.isEmpty){
      return Isogram(letter,false, 'Please input some text!');
    }
    var queryParameters = {
      'letter': letter,
    };

    var url = Uri.https('pangram-yanu.herokuapp.com', '/validIsogram', queryParameters);
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return Isogram.fromJson(convert.jsonDecode(response.body));
    }
    return null;
  }
}

