import 'dart:convert' as convert;
import 'package:go_pangram/models/pangram.dart';
import 'package:http/http.dart' as http;

class PangramRepository {
  Future<Pangram> randomPangram() async {
    var url = 'https://pangram-yanu.herokuapp.com/pangram';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return Pangram.fromJson(jsonResponse[0]);
    }
    return null;
  }
}
