import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_pangram/bloc/anagram_bloc.dart';
import 'package:go_pangram/bloc/random_anagram_bloc.dart';
import 'package:go_pangram/drawer.dart';

class Anagram extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Anagram',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider(
          create: (BuildContext context) => AnagramBloc(),
        ),
        BlocProvider(
          create: (BuildContext context) => RandomAnagramBloc(),
        ),
      ], child: AnagramPage()),
    );
  }
}

class AnagramPage extends StatefulWidget {
  @override
  _AnagramState createState() => _AnagramState();
}

class _AnagramState extends State<AnagramPage> {
  var _randomAnagramBloc;

  final _firstLetterController = TextEditingController();
  final _secondaryLetterController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var _anagramBloc = BlocProvider.of<AnagramBloc>(context);
    _randomAnagramBloc = BlocProvider.of<RandomAnagramBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Anagram Validator'),
      ),
      drawer: DrawerPage(),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Container(
                            padding: EdgeInsets.only(bottom: 6),
                            height: 60,
                            child: FlatButton(
                              color: Colors.blue,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Icon(
                                Icons.compare,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                _randomAnagramBloc.add(GetRandomAnagram());
                              },
                            ),
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 4,
                        child:
                            BlocBuilder<RandomAnagramBloc, RandomAnagramState>(
                                builder: (BuildContext context,
                                    RandomAnagramState state) {
                          if (state is RandomAnagramInitial) {
                            return _firstLetter('');
                          } else if (state is RandomAnagramLoading) {
                            return _loadingFirstLetter();
                          } else if (state is RandomAnagramLoaded) {
                            return _firstLetter(state.randomAnagram.words[0]);
                          }
                          return null;
                        }),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: _secondaryLetterController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide.none,
                  ),
                  filled: true,
                  fillColor: Colors.blue[50],
                  labelStyle: TextStyle(color: Colors.blue[300]),
                  labelText: 'Secondary Letter'),
            ),
            SizedBox(
              height: 15,
            ),
            BlocBuilder<AnagramBloc, AnagramState>(
              builder: (BuildContext context, AnagramState state) {
                if (state is AnagramInitial) {
                  return _checkButton(
                      _anagramBloc, 'Check Anagram', Icons.import_export, '');
                } else if (state is AnagramLoading) {
                  return _loadingButton();
                } else if (state is AnagramLoaded) {
                  if (state.anagram.isAnagram) {
                    return _checkButton(
                        _anagramBloc, 'Valid Anagram', Icons.check, '');
                  } else {
                    return _checkButton(_anagramBloc, 'Invalid Anagram',
                        Icons.close, state.anagram.reason);
                  }
                }
                return null;
              },
            ),
          ],
        ),
      ),
    );
  }

  Column _firstLetter(String value) {
    _firstLetterController.text = value;
    return Column(
      children: <Widget>[
        TextFormField(
          controller: _firstLetterController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide.none,
              ),
              filled: true,
              fillColor: Colors.blue[50],
              labelStyle: TextStyle(color: Colors.blue[300]),
              labelText: 'First Letter'),
        ),
        SizedBox(height: 6,)
      ],
    );
  }

  Column _loadingFirstLetter() {
    return Column(
      children: <Widget>[
        TextFormField(
          style: TextStyle(
            color: Colors.grey
          ),
          controller: _firstLetterController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide.none,
              ),
              filled: true,
              fillColor: Colors.grey[50],
              labelStyle: TextStyle(color: Colors.grey[300]),
              labelText: 'First Letter'),
        ),
        LinearProgressIndicator(),
      ],
    );
  }

  void submitAnagram(AnagramBloc anagramBloc) {
    anagramBloc.add(GetAnagram(
        _firstLetterController.text.trim(), _secondaryLetterController.text.trim()));
  }

  Widget _checkButton(
      AnagramBloc anagramBloc, String arg, IconData icon, String reason) {
    return Column(
      children: <Widget>[
        Container(
          width: 380,
          height: 50,
          child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () {
                submitAnagram(anagramBloc);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(arg),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(icon)
                ],
              )),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
            alignment: Alignment.topLeft,
            child: Text(
              reason,
              style: TextStyle(color: Colors.red),
            )),
      ],
    );
  }

  Widget _loadingButton() {
    return Container(
      width: 380,
      height: 50,
      child: FlatButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          color: Colors.blue[200],
          textColor: Colors.white,
          onPressed: () {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
            ],
          )),
    );
  }
}
