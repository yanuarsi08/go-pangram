part of 'pangram_bloc.dart';

abstract class PangramEvent extends Equatable {
  const PangramEvent();
}

class GetPangram extends PangramEvent{
  @override
  List<Object> get props => [];

}