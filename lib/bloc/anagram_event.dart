part of 'anagram_bloc.dart';

abstract class AnagramEvent extends Equatable {
  const AnagramEvent();
}

class GetAnagram extends AnagramEvent{
  final String firstLetter;
  final String secondaryLetter;

  GetAnagram(this.firstLetter, this.secondaryLetter);

  @override
  List<Object> get props => [];

}