part of 'anagram_bloc.dart'; 

abstract class AnagramState extends Equatable {
  const AnagramState();
}

class AnagramInitial extends AnagramState {
  @override
  List<Object> get props => [];
}

class AnagramLoading extends AnagramState {
  @override
  List<Object> get props => [];
}

class AnagramLoaded extends AnagramState {
  AnagramLoaded(this.anagram);

  @override
  List<Object> get props => [anagram];

  final Anagram anagram;
}