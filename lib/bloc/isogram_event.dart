part of 'isogram_bloc.dart';

abstract class IsogramEvent extends Equatable {
  const IsogramEvent();
}


class GetRandomIsogram extends IsogramEvent{
  @override
  List<Object> get props => [];

}

class GetValidIsogram extends IsogramEvent {
  final String letter;
  final bool yes;

  GetValidIsogram(this.letter, this.yes);
  @override
  List<Object> get props => [];

}