import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:go_pangram/models/random_anagram.dart';
import 'package:go_pangram/repositories/anagram_repository.dart';

part 'random_anagram_event.dart';
part 'random_anagram_state.dart';

class RandomAnagramBloc extends Bloc<RandomAnagramEvent, RandomAnagramState> {
  @override
  RandomAnagramState get initialState => RandomAnagramInitial();

  @override
  Stream<RandomAnagramState> mapEventToState(
    RandomAnagramEvent event,
  ) async* {
    if(event is GetRandomAnagram){
      yield RandomAnagramLoading();
      final randomAnagram = await AnagramRepository().randomAnagram();
      yield RandomAnagramLoaded(randomAnagram);
    }
  }
}
