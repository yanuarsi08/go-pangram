import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:go_pangram/models/pangram.dart';
import 'package:go_pangram/repositories/pangram_repository.dart';

part 'pangram_event.dart';
part 'pangram_state.dart';

class PangramBloc extends Bloc<PangramEvent, PangramState> {
  @override
  PangramState get initialState => PangramInitial();

  @override
  Stream<PangramState> mapEventToState(
    PangramEvent event,
  ) async* {
    if(event is GetPangram){
      yield PangramLoading();
      final pangram = await PangramRepository().randomPangram();
      yield PangramLoaded(pangram);
    }
  }
}
