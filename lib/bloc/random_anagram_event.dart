part of 'random_anagram_bloc.dart';

abstract class RandomAnagramEvent extends Equatable {
  const RandomAnagramEvent();
}

class GetRandomAnagram extends RandomAnagramEvent{
  @override
  List<Object> get props => [];

}