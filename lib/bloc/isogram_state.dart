part of 'isogram_bloc.dart';

abstract class IsogramState extends Equatable {
  const IsogramState();
}

class IsogramInitial extends IsogramState {
  @override
  List<Object> get props => [];
}

class RandomIsogramLoading extends IsogramState {
  @override
  List<Object> get props => [];
}

class RandomIsogramLoaded extends IsogramState {
  RandomIsogramLoaded(this.randomIsogram);

  @override
  List<Object> get props => [randomIsogram];

  final RandomIsogram randomIsogram;
}

class YesValidIsogramLoading extends IsogramState {
  @override
  List<Object> get props => [];
}

class YesValidIsogramLoaded extends IsogramState {
  YesValidIsogramLoaded(this.isogram);

  @override
  List<Object> get props => [isogram];

  final Isogram isogram;
}

class NoValidIsogramLoading extends IsogramState {
  @override
  List<Object> get props => [];
}

class NoValidIsogramLoaded extends IsogramState {
  NoValidIsogramLoaded(this.isogram);

  @override
  List<Object> get props => [isogram];

  final Isogram isogram;
}