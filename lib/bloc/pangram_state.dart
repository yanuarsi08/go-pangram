part of 'pangram_bloc.dart';

abstract class PangramState extends Equatable {
  const PangramState();
}

class PangramInitial extends PangramState {
  @override
  List<Object> get props => [];
}

class PangramLoading extends PangramState {
  @override
  List<Object> get props => [];
}

class PangramLoaded extends PangramState {
  PangramLoaded(this.pangram);

  @override
  List<Object> get props => [pangram];

  final Pangram pangram;
}