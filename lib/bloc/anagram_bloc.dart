import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:go_pangram/models/anagram.dart';
import 'package:go_pangram/repositories/anagram_repository.dart';

part 'anagram_event.dart';
part 'anagram_state.dart';

class AnagramBloc extends Bloc<AnagramEvent, AnagramState> {
  @override
  AnagramState get initialState => AnagramInitial();

  @override
  Stream<AnagramState> mapEventToState(
    AnagramEvent event,
  ) async* {
    if(event is GetAnagram){
      yield AnagramLoading();
      final anagram = await AnagramRepository().isValidAnagram(event.firstLetter, event.secondaryLetter);
      yield AnagramLoaded(anagram);
    }
  }
}
