import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:go_pangram/models/isogram.dart';
import 'package:go_pangram/models/random_isogram.dart';
import 'package:go_pangram/repositories/isogram_repository.dart';

part 'isogram_event.dart';
part 'isogram_state.dart';

class IsogramBloc extends Bloc<IsogramEvent, IsogramState> {
  @override
  IsogramState get initialState => IsogramInitial();

  @override
  Stream<IsogramState> mapEventToState(
    IsogramEvent event,
  ) async* {
    if(event is GetRandomIsogram){
      yield RandomIsogramLoading();
      final randomIsogram = await IsogramRepository().randomIsogram();
      yield RandomIsogramLoaded(randomIsogram);
    }else if(event is GetValidIsogram){
      if(event.yes){
        yield YesValidIsogramLoading();
        final isogram = await IsogramRepository().isValidIsogram(event.letter);
        yield YesValidIsogramLoaded(isogram);
      }else{
        yield NoValidIsogramLoading();
        final isogram = await IsogramRepository().isValidIsogram(event.letter);
        yield NoValidIsogramLoaded(isogram);

      }
    }
  }
}
