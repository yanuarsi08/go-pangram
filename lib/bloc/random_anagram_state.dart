part of 'random_anagram_bloc.dart';

abstract class RandomAnagramState extends Equatable {
  const RandomAnagramState();
}

class RandomAnagramInitial extends RandomAnagramState {
  @override
  List<Object> get props => [];
}

class RandomAnagramLoading extends RandomAnagramState {
  @override
  List<Object> get props => [];
}

class RandomAnagramLoaded extends RandomAnagramState {
  RandomAnagramLoaded(this.randomAnagram);

  @override
  List<Object> get props => [randomAnagram];

  final RandomAnagram randomAnagram;
}
