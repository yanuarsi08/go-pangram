import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_pangram/bloc/pangram_bloc.dart';
import 'package:go_pangram/drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pangram',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
          create: (BuildContext context) => PangramBloc(),
          child: MyHomePage(title: 'Pangram Home Page')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _pangramBloc;

  @override
  Widget build(BuildContext context) {
    _pangramBloc = BlocProvider.of<PangramBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: DrawerPage(),
      body: Center(
        child: BlocBuilder<PangramBloc, PangramState>(
            builder: (BuildContext context, PangramState state) {
          if (state is PangramInitial) {
            return Text('press reload button to get pangram data');
          } else if (state is PangramLoading) {
            return CircularProgressIndicator();
          } else if (state is PangramLoaded) {
            return Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20, 
                vertical: 10
              ),
              child: SingleChildScrollView(
                  child: Text('${state.pangram.pangram}')),
            );
          }
          return null;
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _pangramBloc.add(GetPangram());
        },
        tooltip: 'Reload',
        child: Icon(Icons.refresh),
      ),
    );
  }
}
