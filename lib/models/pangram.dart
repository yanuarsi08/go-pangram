class Pangram {
  final String pangram;

  Pangram(this.pangram);

  factory Pangram.fromJson(dynamic json) {
    return Pangram(
      json['pangram'],
    );
  }
}
