class RandomAnagram {
  final int count;
  final List<String> words;

  RandomAnagram(this.count, [this.words]);

  factory RandomAnagram.fromJson(dynamic json) {
    var wordsObjsJson = json['words'] as List;
    List<String> _words = wordsObjsJson.map((wordJson) => wordJson.toString()).toList();
    return RandomAnagram(json['count'], _words);
  }
}
