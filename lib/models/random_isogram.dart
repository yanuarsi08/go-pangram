class RandomIsogram {
  final String word;
  final bool isIsogram;

  RandomIsogram(this.word, this.isIsogram);

  factory RandomIsogram.fromJson(dynamic json) {
    return RandomIsogram(
      json['word'],
      json['isIsogram'],
    );
  }
}
