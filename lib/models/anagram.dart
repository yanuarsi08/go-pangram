class Anagram {
  final String firstLetter;
  final String secondaryLetter;
  final bool isAnagram;
  final String reason;

  Anagram(this.firstLetter, this.secondaryLetter, this.isAnagram, this.reason);

  factory Anagram.fromJson(dynamic json) {
    return Anagram(
      json['firstLetter'],
      json['secondaryLetter'],
      json['isAnagram'],
      json['reason'],
    );
  }
}
