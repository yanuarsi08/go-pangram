class Isogram {
  final String letter;
  final bool isIsogram;
  final String reason;

  Isogram(this.letter, this.isIsogram, this.reason);

  factory Isogram.fromJson(dynamic json) {
    return Isogram(
      json['letter'],
      json['isIsogram'],
      json['reason'],
    );
  }
}
