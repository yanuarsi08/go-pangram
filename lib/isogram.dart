import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_pangram/bloc/isogram_bloc.dart';
import 'package:go_pangram/drawer.dart';

class Isogram extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Isogram',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (BuildContext context) => IsogramBloc(),
        child: IsogramPage(),
      ),
    );
  }
}

class IsogramPage extends StatefulWidget {
  @override
  _IsogramState createState() => _IsogramState();
}

class _IsogramState extends State<IsogramPage> {
  var _isogramBloc;

  @override
  void initState() {
    super.initState();
    _isogramBloc = BlocProvider.of<IsogramBloc>(context);
    _isogramBloc.add(GetRandomIsogram());
  }

  final _letterController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Isogram Quiz'),
      ),
      drawer: DrawerPage(),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
        child: Column(
          children: <Widget>[
            SizedBox(height: 25,),
            Text('ISOGRAM?', 
              style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue,
              ),
            ),
            SizedBox(height: 40,),
            BlocBuilder<IsogramBloc, IsogramState>(
                builder: (BuildContext context, IsogramState state) {
              if (state is IsogramInitial) {
                return _letter('');
              }
              if (state is RandomIsogramLoading) {
                return _loadingLetter();
              } else if (state is RandomIsogramLoaded) {
                return _letter(state.randomIsogram.word);
              }
              return _initiateLetter();
            }),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                _noButton('NO', Icons.close, false),
                SizedBox(width: 30,),
                _yesButton('YES', Icons.check, true),

              ],
            ),
            SizedBox(
              height: 10,
            ),
            BlocBuilder<IsogramBloc, IsogramState>(
              builder: (BuildContext context, IsogramState state){
                if(state is YesValidIsogramLoaded){
                  return reason(state.isogram.reason, Colors.red);
                }else if(state is NoValidIsogramLoaded){
                  return reason(state.isogram.reason, Colors.green);
                }

                return reason(' ', Colors.black);
              }
              ),
          ],
        ),
      ),
    );
  }

  Container reason(String value, Color color) {
    return Container(
            alignment: Alignment.topLeft,
            child: Text(value, 
              style: TextStyle(
                color: color
                ),
              ),
            );
  }

  BlocBuilder<IsogramBloc, IsogramState> _yesButton(
      String initiateValue, IconData initiateIcon, bool buttonValue) {
    return BlocBuilder<IsogramBloc, IsogramState>(
        builder: (BuildContext context, IsogramState state) {
      if (state is YesValidIsogramLoading) {
        return _loadingButton(1);
      } else if (state is YesValidIsogramLoaded) {
        if (state.isogram.isIsogram == buttonValue) {
          return _checkButton('RIGHT', Icons.thumb_up, Colors.green, true);
        } else {
          return _checkButton('WRONG', Icons.thumb_down, Colors.red, true);
        }
      }

      return _checkButton(initiateValue, initiateIcon, Colors.blue, true);
    });
  }

  BlocBuilder<IsogramBloc, IsogramState> _noButton(
      String initiateValue, IconData initiateIcon, bool buttonValue) {
    return BlocBuilder<IsogramBloc, IsogramState>(
        builder: (BuildContext context, IsogramState state) {
      if (state is NoValidIsogramLoading) {
        return _loadingButton(1);
      } else if (state is NoValidIsogramLoaded) {
        if (state.isogram.isIsogram == buttonValue) {
          return _checkButton('RIGHT', Icons.thumb_up, Colors.green, false);
        } else {
          return _checkButton('WRONG', Icons.thumb_down, Colors.red, false);
        }
        
      }

      return _checkButton(initiateValue, initiateIcon, Colors.blue, false);
    });
  }

  Column _initiateLetter() {
    return Column(
      children: <Widget>[
        TextFormField(
          controller: _letterController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide.none,
            ),
            filled: true,
            fillColor: Colors.blue[50],
            labelStyle: TextStyle(color: Colors.blue[300]),
          ),
        ),
        SizedBox(
          height: 6,
        )
      ],
    );
  }

  Column _letter(String value) {
    _letterController.text = value;
    return _initiateLetter();
  }

  Column _loadingLetter() {
    return Column(
      children: <Widget>[
        TextFormField(
          style: TextStyle(color: Colors.grey),
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide.none,
            ),
            filled: true,
            fillColor: Colors.grey[50],
            labelStyle: TextStyle(color: Colors.grey[300]),
          ),
        ),
        LinearProgressIndicator(),
      ],
    );
  }

  Widget _checkButton(String arg, IconData icon,Color color, bool yes) {
    return Expanded(
      child: Container(
        width: 380,
        height: 50,
        child: FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: color,
            textColor: Colors.white,
            onPressed: () async {
              await _isogramBloc.add(GetValidIsogram(_letterController.text, yes));
              await Future.delayed(Duration(seconds: 3));
              await _isogramBloc.add(GetRandomIsogram());
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(arg),
                SizedBox(
                  width: 10,),
                Icon(icon),
              ],
            )),
      ),
    );
  }

  Widget _loadingButton(int flex) {
    return Expanded(
      flex: flex,
      child: Container(
        width: 380,
        height: 50,
        child: FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Colors.blue[200],
            textColor: Colors.white,
            onPressed: () {},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              ],
            )),
      ),
    );
  }
}
