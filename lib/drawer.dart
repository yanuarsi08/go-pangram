import 'package:flutter/material.dart';
import 'package:go_pangram/anagram.dart';
import 'package:go_pangram/isogram.dart';
import 'package:go_pangram/main.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Node.js Training Menu',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
                Text(
                  'yanuar@infoflow.co.id',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
                Text(
                  'yanuar.adika@dkatalis.com',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.refresh),
            title: Text('Random Pangram'),
            onTap: () {
              _buildOnTapPage(context, MyApp());
            },
          ),
          ListTile(
            leading: Icon(Icons.compare_arrows),
            title: Text('Anagram Validator'),
            onTap: () {
              _buildOnTapPage(context, Anagram());
            },
          ),
          ListTile(
            leading: Icon(Icons.category),
            title: Text('Isogram Quiz'),
            onTap: () {
              _buildOnTapPage(context, Isogram());
            },
          ),
        ],
      ),
    );
  }

  void _buildOnTapPage(BuildContext context, Object object) {
    Navigator.of(context).pushReplacement(new PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => object,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return child;
        }));
  }
}
